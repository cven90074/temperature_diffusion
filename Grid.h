#ifndef GRID_H
#define GRID_H

#include <iostream>
#include <cmath>
#include <vector>
#include <string>

class Grid {
public:
    Grid(int rows, int columns, float default_value );
    ~Grid();
    int Rows() const;
    int Columns() const;
    float& operator()(int i, int j);
    const float& operator()(int i, int j) const;
    void Out();
    void SaveResultsToVti(std::string file_name, std::string prop_name);
    void SmoothingResults(int cycles);

private:
    std::vector<float> data_;
    const int rows_, cols_;
};

#endif