//
// Created by Cuong Bui & Amila on 6/03/2023.
//

#ifndef RANDOMWALK_WALKER_H
#define RANDOMWALK_WALKER_H
#include <utility>
#include <vector>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <algorithm>
#include <map>
#include "Grid.h"

using namespace std;

class Walker { // Class Walker to perform the random walk on 2D grid
private:
    pair<int,int> pos;
public:
    Walker(); //Constructor
    Walker(pair<int,int> pos); //Constructor
    ~Walker(); //De-constructor
    bool operator!=(Walker &v) const;//Function to return boolean value operator to compare two "Walker" objects based on their positions
    //vector<Walker> RandomWalk (int& nx, int& ny) const; //Function to make random walk
    pair<bool, double> RandomWalk(const Grid& t_grid, const Grid& k_grid) const;
    void PrintWalker() const; // print the position of walker
    int GetI() const;//return the row indices of the current position
    int GetJ() const;//return the column indices of the current position
};


#endif //RANDOMWALK_WALKER_H
