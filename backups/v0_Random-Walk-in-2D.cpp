//--------------------------------------------------------------------------------------------------------------//
// v0_Random-Walk-in-2D.cpp : This file contains the 'main' function. Program execution begins and ends there.
//CVEN90074-SM1-2023-Computational Geotechnical Engineering
// Group (Qusi $ Amina)
//8 March 2023
//---------------------------------------------------------------------------------------------------------------//

//-------------------------------------------------------START-----------------------------------------------------//

#include <iostream> //provides input/output stream functionality
#include <cstdlib>  //provides functions for generating random numbers
#include <ctime>    //provides functionality for working with time and dates
#include <vector>   //provides dynamic array functionality

using namespace std;

int main()
{   // Build 10x10 Grid
    const int ROWS = 10;
    const int COLS = 10;
    int grid[ROWS][COLS] = { 0 }; // initialize grid to all zeros

    //position initializing
    int x = COLS / 2; // starting position at center of grid
    int y = ROWS / 2;
    grid[y][x] = 1; // starting position marking

    srand(time(0)); // seed random number generator

    vector<pair<int, int>> visited; // vector to store visited positions to avoid repeated walks

    for (int i = 0; i < 100; i++) { // take 100 random steps
        double r = (double)rand() / RAND_MAX; // generate a random number between 0 and 1 after normalizing by the max. possible random number.
        int direction; // initialize direction variable

        // Choose the probability of each movement direction 
        if (r < 0.2) { // 20% chance of moving up
            direction = 0;
        }
        else if (r < 0.35) { // 15% chance of moving right
            direction = 1;
        }
        else if (r < 0.7) { // 35% chance of moving down
            direction = 2;
        }
        else { // 30% chance of moving left
            direction = 3;
        }

        pair<int, int> next_pos; // initialize next position variable
        switch (direction) {
        case 0: // move up
            if (y > 0) {
                next_pos = make_pair(x, y - 1);
            }
            break;
        case 1: // move right
            if (x < COLS - 1) {
                next_pos = make_pair(x + 1, y);
            }
            break;
        case 2: // move down
            if (y < ROWS - 1) {
                next_pos = make_pair(x, y + 1);
            }
            break;
        case 3: // move left
            if (x > 0) {
                next_pos = make_pair(x - 1, y);
            }
            break;
        }
        //cout << direction;


        if (next_pos.first >= 0 && next_pos.first < COLS && next_pos.second >= 0 && next_pos.second < ROWS) { // check if within bounds
            bool already_visited = false;
            for (auto pos : visited) { // check if position has already been visited
                if (pos == next_pos) {
                    already_visited = true;
                    break;
                }
            }
            if (!already_visited) { // move to the position if it has not been visited
                x = next_pos.first;
                y = next_pos.second;
                visited.push_back(make_pair(x, y));
                grid[y][x]++; // new position marking
            }
        }
    }


    // print out the grid with the path
    for (int r = 0; r < ROWS; r++) {
        for (int c = 0; c < COLS; c++) {
            if (grid[r][c] > 0) {
                cout << "X ";
            }
            else {
                cout << ". ";
            }
        }
        cout << endl;
    }

    return 0;


}

//-------------------------------------------------------END-------------------------------------------------------//
