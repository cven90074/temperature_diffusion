#include "Grid.h"
#include "Walker.h"
#include <iostream>
#include <omp.h>
#include <fstream>

using namespace std;

int main(){


    //------------------
    // 1. build grids
    //------------------
    //grid dimension
    const int ROWS = 50;
    const int COLS = 50;

    //thermal conductivity 
    float K1 = 1; //<-- the thermal conductivity (W/k) of Basalt   ( source: chatGPT)
    float K2 = 1.5; // <-- the thermal conductivity (W/k) of Quartz ( source: chatGPT)

    //temperature
    float Temp = 0.;

    Grid temperature_grid(ROWS, COLS, Temp); //define temperature grid 
    Grid conductivity_grid(ROWS, COLS, K1);  //define conductivity grid 

    //---------------------------
    // 2. assgin grid properties
    //---------------------------
    //giving K2 value to the quartz section
    for (int i = 12; i <= 37; i++) {
        for (int j = 17; j <= 33; j++) {
            conductivity_grid(i, j) = K2;
        }
    }
    //output grid
    //conductivity_grid.Out();
    conductivity_grid.SaveResultsToVti("input_thermal_conductivity", "thermal_conductivity");

    // give temperature values to top and bottom boundaries of temperature grid
    int bottom_row = 0;
    int top_row = temperature_grid.Rows() - 1;
    for (int j = 0; j < temperature_grid.Columns(); j++) {
        temperature_grid(top_row, j) = 25.;
        temperature_grid(bottom_row, j) = 5.;
    }
    //output grid
    //temperature_grid.Out();
    temperature_grid.SaveResultsToVti("input_temperature", "temperature");


    //----------------------------------------------
    // 3. apply random walker to each of grid cells
    //----------------------------------------------
    int target_walks(500); //number of valid (hitting top/bottom) random walks for each cell
    //looping over internal grid cells (excluding boundary cells)
    #pragma omp parallel for collapse(2)
    for (int i{ 1 }; i < ROWS-1; i++) {
        for (int j{ 0 }; j < COLS ; j++) {
            int valid_walks(0), total_walks(0);
            double sum_results(0.);
            //perform random walks until the target count is reached
            do {
                Walker walker{ pair<int,int>(i,j) }; //make a walker starting from current grid cell
                pair<bool,double> results = walker.RandomWalk(temperature_grid, conductivity_grid); //perform a random walk
                if (results.first) { //only valid if the walker reached top or bottom boundaries
                    sum_results += results.second;
                    valid_walks++;
                }
                total_walks++;
            } while (valid_walks < target_walks);
            double mean_result = sum_results / static_cast<double>(valid_walks); //get averaged result
            temperature_grid(i, j) = mean_result; //store result on grid cell

            //int tid = omp_get_thread_num();
            //printf("The thread %d  executes i = %d\n", tid, i);
            cout << "i = " << i << ", j = " << j << ", t = " << mean_result << ", num of walks = " << total_walks << endl;
        }
    }

    //output results
    //temperature_grid.Out();
    temperature_grid.SaveResultsToVti("temperature_results", "temperature");

    //smoothing results
    temperature_grid.SmoothingResults(1);
    temperature_grid.SaveResultsToVti("smoothed_temperature_results_cycle1", "temperature");
    temperature_grid.SmoothingResults(1);
    temperature_grid.SaveResultsToVti("smoothed_temperature_results_cycle2", "temperature");
    temperature_grid.SmoothingResults(1);
    temperature_grid.SaveResultsToVti("smoothed_temperature_results_cycle3", "temperature");

    return 0;
}
    
