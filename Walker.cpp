//
// Created by Cuong Bui & Amila on 6/03/2023.
//

#include "Walker.h"
#include "Grid.h"
#include <random>
#include <chrono>
#include <cassert>


// Default constructor: initializes walker at (0, 0)
Walker::Walker(){
    this-> pos = pair<int,int>(0,0);

}
// Constructor with initial position provided as argument
Walker::Walker(pair<int,int> pos){
    this->pos = pos; //pair<int,int> pos: current position of the walker (x,y)
}
// Default destructor
Walker::~Walker()= default;

// Returns the first element of the position pair (the x-coordinate)
int Walker::GetI() const{
    return (this->pos).first;
}

// Returns the second element of the position pair (the y-coordinate)
int Walker::GetJ() const{
    return (this->pos).second;
}


// Simulates a random walk of a walker on a grid of size nx by ny,
// starting at the walker's initial position.
// Returns a vector containing all of the walker's positions during the walk.

//vector<Walker> Walker::RandomWalk(int& nx, int& ny) const {
pair<bool, double> Walker::RandomWalk(const Grid& t_grid, const Grid& k_grid ) const {
    int cols = t_grid.Columns();
    int rows = t_grid.Rows();

    if (cols != k_grid.Columns() or rows != k_grid.Rows())
        throw::logic_error("temperature grid and thermal conductivity grid must have the same dimensions");


    int newi = this->pos.first; //row index
    int newj = this->pos.second; //column index


    vector<Walker> vW; //recording history walkers 
    // Keep walking until the walker hits the boundary of the grid
    do {
        int dir; //dir: direction, the probability for each direction is scaled with its thermal conductivity value

        //generate of random number between 0 and 1
        //srand((unsigned)time(NULL));
        //double ran = (double) rand() / RAND_MAX;// Generate a random number between 0 and 1
        
        std::mt19937_64 rng;
        uint64_t timeSeed = std::chrono::high_resolution_clock::now().time_since_epoch().count();
        std::seed_seq ss{ uint32_t(timeSeed & 0xffffffff), uint32_t(timeSeed >> 32) };
        rng.seed(ss);
        std::uniform_real_distribution<double> unif(0, 1);
        double ran = unif(rng);
        
        //determine probability based on thermal conductivity values
        
        double k_right = ((newj+1) < cols) ? k_grid(newi, newj + 1) : 0.;
        double k_left = ((newj-1) >= 0) ? k_grid(newi, newj - 1) : 0.;
        double k_up = ((newi - 1) >= 0) ? k_grid(newi - 1, newj) : 0;
        double k_down = ((newi + 1) < rows) ? k_grid(newi + 1, newj) : 0;
        double k_sum = k_right + k_left + k_up + k_down;
        double p_right = k_right / k_sum;
        double p_left = k_left / k_sum;
        double p_up = k_up / k_sum;
        double p_down = k_down / k_sum;
        //determine moving direction
        if (ran < p_right) {
            dir = 0; // Move right
        } else if (ran < (p_right + p_left)) {
            dir = 1; // Move left
        } else if (ran < (p_right + p_left + p_up)) {
            dir = 2; // Move up
        } else {
            dir = 3; // Move down
        }

        // Take a step in the chosen direction, if possible
        switch (dir) {
            case 0: // Move right (j+1)
                if ((newj + 1) < cols) { // Check if walker is within grid bounds
                    int s = vW.size(); // Number of steps taken so far
                    Walker nextWalk{pair<int,int>(newi, newj+1)}; // Create new walker at next position
                    
                    // If this is the first or second step, or the next position is different from
                    // the second-to-last position, add the new step to the vector
                    
                    if(s<2){
                        vW.push_back(nextWalk);
                        newj++;
                    } else {
                        Walker frontWalk = vW[s-2]; // Get the second-to-last position
                        if(nextWalk!=frontWalk){ // Check if next position is different
                            vW.push_back(nextWalk);
                            newj++;
                        }
                    }
                }
                break;
            case 1: // Move left (j-1)
                if ((newj - 1) >= 0) {
                    int s = vW.size();
                    Walker nextWalk{pair<int,int>(newi, newj-1)};
                    if(s<2){
                        vW.push_back(nextWalk);
                        newj--;
                    } else {
                        Walker frontWalk = vW[s-2];
                        if(nextWalk!=frontWalk){
                            vW.push_back(nextWalk);
                            newj--;
                        }
                    }
                }
                break;
            case 2: // Move up (i-1)
                if ((newi - 1) >= 0) {
                    int s = vW.size();
                    Walker nextWalk{pair<int,int>(newi-1, newj)};
                    if(s<2){
                        vW.push_back(nextWalk);
                        newi--;
                    } else {
                        Walker frontWalk = vW[s-2];
                        if(nextWalk!=frontWalk){
                            vW.push_back(nextWalk);
                            newi--;
                        }
                    }
                }
                break;
            case 3: // Move down (i+1)
                if ((newi + 1) < rows) {
                    int s = vW.size();
                    Walker nextWalk{pair<int,int>(newi+1, newj)};
                    if(s<2){
                        vW.push_back(nextWalk);
                        newi++;
                    } else {
                        Walker frontWalk = vW[s-2];
                        if(nextWalk!=frontWalk){
                            vW.push_back(nextWalk);
                            newi++;
                        }
                    }
                }
                break;
        }
    } while ((newj != 0) && (newj != cols-1) && (newi != 0) && (newi != rows - 1));

    bool valid(false);
    double result=NAN;
    assert(isnan(result));
    if (newi == 0 or newi == rows - 1) { //reached top or bottom row
        valid = true;
        result = t_grid(newi, newj);
    }

    return make_pair(valid, result);
}



// Define member function PrintWalker()
void Walker::PrintWalker() const {
    cout << "(" << this->pos.first << ", " << this->pos.second << ")"; // Output the position of the walker in the form (i, j)
}

// Define operator != for comparing two Walkers
bool Walker::operator!=(Walker &v) const{
    
    // Check if the i-coordinate of this Walker is different from the i-coordinate of the other Walker AND
    // the j-coordinate of this Walker is different from the j-coordinate of the other Walker
    
    if((this->GetI()!=v.GetI())&&(this->GetJ()!=v.GetJ())){
        return true; // If both coordinates are different, return true to indicate that the Walkers are not equal
    } else {
        return false; // If either coordinate is the same, return false to indicate that the Walkers are equal
    }
}




