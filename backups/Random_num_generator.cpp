// Random_num_generator.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include<iostream>
#include <random>
#include<functional>
using namespace std;
int main()
{
    using my_engine = default_random_engine; // type of engine
    using my_distribution = uniform_real_distribution<float>; // type of distribution
    //my_engine re{}; // the default engine
    //my_distribution zero_to_four{ 0,4 }; // distribution that maps to the floats  0..4
    auto die = bind(uniform_real_distribution<>{0, 4}, default_random_engine{});
    //float x = die();
    for (int i = 0; i < 4; i++) {
        cout << die() << "\n";
    }
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
