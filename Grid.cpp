#include "Grid.h"
#include <iostream>
#include <cmath>
#include <vector>
#include <cassert>
#include <fstream>

using namespace std;


Grid::Grid(int rows, int columns, float default_value )
    : data_( rows * columns, default_value ),
      rows_(rows),
      cols_(columns)
{
}
    
Grid::~Grid()
{
}


float& Grid::operator()(int i, int j)
{
    assert(i < rows_);
    assert(j < cols_);
    assert( i >= 0 && j >= 0 );

    return data_[i * cols_ + j];
}

const float& Grid::operator()(int i, int j) const
{
    assert(i < rows_);
    assert(j < cols_);
    assert(i >= 0 && j >= 0);

    return data_[i * cols_ + j];
}

void Grid::Out() {
    for (auto i{ 0U }; i < rows_; i++) {
        for (auto j{ 0U }; j < cols_; j++) {
            cout << data_[i * cols_ + j] << " ";
        }
        cout << endl;
    }
}


int Grid::Rows() const { return rows_;  }

int Grid::Columns() const { return cols_; }


void Grid::SaveResultsToVti(string file_name, string prop_name)
{
    file_name += ".vti";
    ofstream out_file(file_name.c_str());
    assert(out_file.is_open());

    out_file << "<VTKFile type=\"ImageData\" version=\"1.0\" byte_order=\"LittleEndian\" header_type=\"UInt64\">" << endl;
    out_file << "<ImageData WholeExtent=\"0 " << cols_ << " 0 " << rows_ << " 0 " << 0 << "\" Origin=\"" << 0 << " " << 0 << " " << 0 << "\" Spacing=\""\
        << 1 << " " << 1 << " " << 1 << "\">" << endl;
    out_file << "<Piece Extent=\"0 " << cols_ << " 0 " << rows_ << " 0 " << 0 << "\">" << endl;
    out_file << "<CellData>" << endl;

    //cell data
    out_file << "<DataArray type=\"Float32\" Name=\"" << prop_name << "\" format=\"ascii\">" << endl;
    for (int i = 0; i < rows_; i++) {
        for (int j = 0; j < cols_; j++) {
            out_file << data_[i * cols_ + j] << " ";
        }
        out_file << endl;
    }
    out_file << endl;
    out_file << "</DataArray>" << endl << endl;
    //
    out_file << "</CellData>" << endl;
    out_file << "</Piece>" << endl;
    out_file << "</ImageData>" << endl;
    out_file << "</VTKFile>" << endl;

    // close file
    out_file.close();
}

void Grid::SmoothingResults(int cycles) {
    //make a copy of original results
    auto ori_data{ data_ };
    for (int n{ 0 }; n < cycles; n++) {
        for (int i{ 1 }; i < rows_ - 1; i++) { //ignore top and bottom boundary cells with dirichlet conditions
            for (int j{ 0 }; j < cols_; j++) {
                //get sum of values in the neighborhood
                double sum_values(0.);
                int count(0);
                for (int ni{ i - 1 }; ni <= i + 1; ni++) {
                    if (ni >= 0 && ni < rows_) {
                        for (int nj{ j - 1 }; nj <= j + 1; nj++) {
                            if (nj >= 0 && nj < cols_) {
                                sum_values += ori_data[ni * cols_ + nj];
                                count++;
                            }
                        }
                    }
                }
                //averaged value
                double mean_value = sum_values / static_cast<double>(count);
                data_[i * cols_ + j] = mean_value;
            }
        }
    }
}

