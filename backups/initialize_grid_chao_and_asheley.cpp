pair<Grid,Grid> initialize_grid()
{
    std::cout << "Temperature Calcalating on grid!\n";

float K1 = 1; //<-- the thermal conductivity (W/k) of Basalt   ( source: chatGPT)
float K2 = 1.5; // <-- the thermal conductivity (W/k) of Quartz ( source: chatGPT)


// set the grid dimension
const int ROWS = 50;
const int COLS = 50;


Grid temperature_grid(ROWS, COLS, 0.); //define temperature grid 
Grid Conductivity_grid(ROWS, COLS, K1);  //define conductivity grid 

// giving K value to the quartz section
for (int i = 25; i <= 50; i++) {
    for (int j = 17; j <= 33; j++) {
        std::cout << Conductivity_grid[i][j] << K2;
    }

}

//giving K value to basalt section 1
for (int i = 0; i <= 25; i++) {
    for (int j = 0; j <= 50; j++) {
        std::cout << Conductivity_grid[i][j] << K1;
    }
}
// giving K value to the basalt section 2
for (int i = 25; i <= 50; i++) {
    for (int j = 0; j <= 17; j++)
    {
        std::cout << Conductivity_grid[i][j] << K1;
    }
}
// giving K value to the basalt section 3
for (int i = 25; i <= 50; i++) {
    for (int j = 33; j <= 50; j++) {
        std::cout << Conductivity_grid[i][j] << K1;
   
    
    return make_pair(temperature_grid, Conductivity_grid);
    
} //end of fumction